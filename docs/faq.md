# Questions fréquentes

## Comment nous contacter ?
En vous inscrivant sur [notre forum Tchap](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr) (si vous êtes enseignante ou enseignant), en ouvrant [un ticket](https://forge.apps.education.fr/docs/support/-/issues) (si vous êtes déjà membre de la forge) ou en nous envoyant directement un mail à `forge-communs-numeriques@ldif.education.gouv.fr`.

## Que contient la forge des communs numériques éducatifs ?
Comme son nom l'indique, la forge est centrée sur le numérique éducatif. Elle contient des ressources pédagogiques, logiciels et ressources éducatives libres, utiles à la communauté scolaire.

## Qu'est-ce que LaForgeÉdu peut faire pour vous ?
- La forge vous propose des dizaines de logiciels et ressources éducatives, créées par et pour la communauté scolaire, sans contrainte d'utilisation et de partage car sous licence libre : vous trouverez sûrement chaussures à votre pied ! On vous encourage à nous rejoindre et participer mais, bien entendu, aucun obligation de vous inscrire à LaForgeÉdu pour les utiliser.
- Si vous êtes un enseignant développeur ou une enseignante développeuse, le forge vous offre un espace pour y déposer et partager votre code, un peu comme GitHub mais libre et souverain (voir [quelques applications exemples](/cartographie#logiciels-libres)).
- Si vous souhaitez partager du contenu pédagogique sur un site web, LaForgeÉdu offre des fonctions d'hébergement et de rédaction collaborative du contenu (voir [les sites en exemple](/cartographie#ressources-educatives-libres)).
- En rejoignant la forge, vous pourrez bénéficier de la dynamique communautaire qui se met petit à petit en place, en invitant ses membres à interagir et participer aux projets des uns et des autres.
- Vous pourrez être éventuellement invité.e à des événements pour y présenter votre projet en y rencontrant la communauté (La Journée du Libre Éducatif, Ludovia, Educatech...) et participer à des _forgeathon_, sprint de contribution collective aux projets de la forge
- Vous pourrez également bénéficier de la visibilité de la forge en terme de communication.
- Vous pourrez enfin bénéficier de _l'appel à communs_, dispositif qui se met en place pour accompagner, soutenir et valoriser les projets de la forge et leurs communautés.

## Qu'est-ce que vous pouvez faire pour LaForgeÉdu ?
- Autour de vous, dans vos réseaux, en formation... vous nous aideriez à faire connaitre LaForgeÉdu et ses projets, en soulignant notamment leurs spécificités (des _communs numériques_ créés et partagés par les enseignants et leurs communautés)
- Vous pouvez également aider les porteurs de projets à enrichir et améliorer leurs ressources : en interagissant avec eux [via notre groupe de test](https://docs.forge.apps.education.fr/communaute/test/) (rapport de bug, retour d'expériences, suggestion d'améliorations...) voire en collaborant à leurs projets
- Vous pouvez aussi animer avec nous les espaces d'échange et de communication, notamment les forums Tchap dédiés dont [celui d'accueil et d'information générale](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr).
- Enfin vous pouvez carrément rejoindre l'équipe de LaForgeÉdu, ce ne sont pas les besoins qui manquent en animation, documentation, formation, communication, expertise technique, etc. Et nul besoin d'etre un _geek_ pour participer. D'avance merci, si tel est le cas ;) N'hésitez pas à nous contacter à forge-communs-numeriques@ldif.education.gouv.fr

## Qui peut s'inscrire et participer à la Forge des communs numériques éducatifs ?
La forge est un service numérique partagé de [apps.education.fr](https://portail.apps.education.fr/). Apps étant relié à l'annuaire fédéré des profs et des agents, tout le personnel du ministère de l'Éducation nationale peut participer à la forge via son compte Apps. Donc pour qu'une ou un collègue s'inscrive et participe à la forge, il lui faut (si ça n'est déjà fait) simplement [activer son compte Apps](https://portail.apps.education.fr/) avec son mail professionnel (et cliquer sur le service de la forge depuis le portail Apps). Vous pouvez aussi vous connecter directement en passant par [cette page](https://forge.apps.education.fr/users/sign_in).

Mais la forge est également ouverte et accueillante aux membres hors ministère, du moment que les projets sont libres et utiles à notre communauté scolaire. Ces membres peuvent être des professeurs retraités, des enseignants et chercheurs/seuses d'université, d'autres organisations publiques comme l'Inria, des acteurs privés, notamment de l'Edtech, collaborant sur nos projets, des associations et acteurs des communs, etc. Ici l'inscription se fait sur un deuxième annuaire et est soumise à une validation préalable (pour des questions de responsabilité et sécurité, pour voir aussi si les projets qui candidatent s'inscrivent bien dans le numérique éducatif libre). Si vous souhaitez inviter des personnes hors du ministère à collaborer sur vos projets, c'est donc possible.

## Comment nommer un groupe ou un projet ?
La forge est un espace de liberté pour ses membres. Mais nous souhaitons attirer votre attention sur le choix du nom d'un groupe que vous décideriez de créer pour ensuite l'utiliser en _GitLab Pages_.

Si vous choisissez "Ministère" alors votre application en ligne ou site web personnel sera accessible à l'URL `ministere.forge.apps.education.fr` Cet exemple est caricatural mais vous comprenez aisément que cela puisse poser problème et créer de la confusion. De meme "Troisième", "Mathématiques" ou "Tutoriels" qui pourraient laisser à penser qu'on y retrouvera toutes les ressources de troisième et de mathématiques de la forge, ou des tutoriels concernant l'usage de la forge.

Merci donc de personnaliser et de ne pas utiliser de noms génériques pour vos groupes : soyez le plus précis possible. Par exemple, si votre lycée s'appelle Van Gogh, ne mettez pas seulement vanGogh, mais par exemple lyceeVanGoghErmont (lycée Van Gogh d'Ermont)

Nous vous demandons donc de respecter les règles suivantes :

* Utiliser un nom de groupe qui soit clair et précis, et qui ne soit pas trompeur quant à l'identité ou les objectifs du groupe.
* Ne pas choisir un nom de groupe qui pourrait créer une confusion avec un autre groupe existant ou qui pourrait nuire à la réputation d'autrui.
* Les noms de groupe ne doivent en aucun cas contenir des éléments offensants, discriminatoires, ou qui pourraient gêner d'autres utilisateurs.
* Les noms de groupe trop génériques, tels que "modèles" ou "cours", sont interdits /a priori/ afin de prévenir toute confusion et de maintenir une organisation claire au sein de la plateforme. Ils peuvent toutefois être utilisés, après accord par le comité de suivi (COSUI) s'ils représentent une communauté de contributeurs.

Le [comité de suivi](https://docs.forge.apps.education.fr/apropos.html) se réserve le droit de juger qu'un nom de groupe n'est pas approprié et de vous demander d'en changer. Merci de votre compréhension. Certains groupes aux noms génériques existent et sont liés à l'existence d'une communauté active sur le sujet.

## Comment faire pour s'inscrire lorsque l'on n'est pas du ministère de l'Éducation nationale ?
Si vous n'êtes pas du ministère et souhaitez ouvrir un compte externe sur la forge, merci de nous contacter par mail à l'adresse `forge-communs-numeriques@ldif.education.gouv.fr` en vous présentant succinctement et en justifiant votre candidature (à quels projets libres existants comptez-vous participer ? quels nouveaux projets libres comptez-vous partager avec notre communauté ?), par exemple :

> Bonjour,    
> je suis XXX, et je souhaiterais obtenir un compte sur la Forge des communs numériques éducatifs pour
> 
> - participer au projet XXX
> - ET/OU héberger le projet XXX (rapide description).
> 
> Cordialement,    
> XXX

Nous vous répondrons dans les plus brefs délais.

## Quelle licence choisir pour mon projet sur la forge ?

Commençons par rappeler que tout projet de LaForgeÉdu est un logiciel **libre** ou une ressource éducative **libre**. Et donc tout projet doit s'accompagner à la racine du dépôt d'un fichier nommé LICENSE explicitant la licence choisie (ex. [celui de MathALEA](https://forge.apps.education.fr/coopmaths/mathalea/-/blob/main/LICENSE)).

- **Si votre projet est un logiciel libre** (i.e. du code) alors il conviendra d'en choisir la licence parmi [cette liste](https://www.data.gouv.fr/fr/pages/legal/licences/) du site data.gouv. Si vous choisissez une licence _permissive_, nous vous suggérons la MIT. Si vous préférez une licence _à réciprocité_ (copyleft), nous vous suggérons la GPLv3. Liens vers le site de l'ANCT pour en savoir plus [sur les licences libres en général](https://licence-libre.incubateur.anct.gouv.fr/) et [sur la distinction importante entre les licences permissives et les licences copyleft](https://licence-libre.incubateur.anct.gouv.fr/licence-libre/le-point-sur-les-licences-libres) en particulier.

- **Si votre projet est une ressource éducative libre** (par ex. du contenu pédagogique sur un site web), il conviendra d'en choisir la licence parmi les différents types de licence Creative Commons. Nous préférons la CC-BY et nous déconseillons d'introduire la clause ND (car l'éducation est en perpétuelle évolution). Pour orienter votre choix, nous vous suggérons ces quatre lectures : le témoignage d'un enseignant [Pourquoi je publie mes travaux sous licence libre](https://ababsurdo.fr/blog/20141119-pourquoi-publier-sous-licence-libre/), ces deux articles issus de blog de la chaire RELIA de l'Unesco [Choisir une licence ouverte, une affaire de goût ou de posture ?](https://chaireunescorelia.univ-nantes.fr/2023/06/14/choisir-une-licence-ouverte-une-affaire-de-gout-ou-de-posture/) et [Comment garantir les deux “R” qui fâchent…](https://chaireunescorelia.univ-nantes.fr/2024/10/30/comment-garantir-les-deux-r-qui-fachent/) ainsi que [La connaissance libre grâce aux licences Creative Commons, ou pourquoi la clause « pas d’utilisation commerciale » ne répond pas (forcément) à vos besoins](https://upload.wikimedia.org/wikipedia/commons/0/0b/WMBE-La_connaissance_libre_gr%C3%A2ce_aux_licences_Creative_Commons.pdf) de Wikimédia Belgique.

## Comment suivre l'actualité de la forge ?
Si vous etes enseignant ou agent de la fonction publique, nous vous invitons à vous inscrire sur notre [salon Tchap dédié](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.agriculture.tchap.gouv.fr) pour suivre l'actualité de la forge et notamment les annonces des nouveautés et mises à jour.

Vous pouvez aussi suivre le compte @LeLibreEdu [sur Mastodon](https://mastodon.mim-libre.fr/@lelibreedu) et/ou [sur X](https://twitter.com/LeLibreEdu). Et lire tous les deux mois la [Gazette des communs numériques éducatifs](https://gazette.forge.apps.education.fr/).

## Comment inviter la communauté de LaForgeÉdu à contribuer à mon projet ?
Nous vous suggérons d'inviter les membres à ouvrir des tickets (issues) dans l'espace dédié de votre projet (afin de raporter un bug, suggérer une amélioration, etc.). C'est souvent le premier pas vers la collaboration (cf [cette capsule vidéo](https://tube-sciences-technologies.apps.education.fr/w/1FHx5ntDrd9mwo5k6dAB2F) sur la création de tickets).

Il est également possible de créer un forum au nom de votre projet sur la messagerie instantanée [Tchap](https://www.tchap.gouv.fr/) et inviter les utilisateurs à y participer.

## Qui compose la communauté de LaForgeÉdu ? Quelle gouvernance de LaForgeÉdu ?
LaForgeÉdu mutualise et fédère des projets de communs numériques. Mais elle souhaite également être elle-même un **commun numérique** dans son approche, sa culture et sa gouvernance : un _« commun des communs »_ en quelque sorte. Pour reprendre la typologie/posture de [la Fabrique des GéoCommuns de l'IGN](https://www.ign.fr/publications-de-l-ign/institut/kiosque/publications/2023_01_dossier_des_communs.pdf), LaForgeÉdu compte 5 principales catégories d'acteurs : le membre, le contributeur, l'opérateur, le sponsor et le garant.

Le/la **membre** y utilise ses ressources, s’exprime dans les forums (Tchap), ouvre des tickets pour rapporter un bug, suggère une amélioration et fait remonter les besoins.
Rôle évidemment fondamental, le/la **contributeur**/trice crée ou participe à un ou plusieurs communs de la forge.
L'**opérateur** est le coeur de pilotage de LaForgeÉdu. Il administre le service, rédige sa documentation et anime la communauté. Il est composé de personnes de la DNE (Direction du numérique pour l'éducation du ministère), des DANE de Lyon et de Grenoble, de l'association AEIF et d'enseignants.
La DNE est le **sponsor** de la forge. Elle met à disposition et maintient l'instance GitLab sur laquelle repose la forge. Elle veille à ce que LaForgeÉdu soit bien intégrée avec la stratégie globale du numérique éducatif. Elle accompagne et valorise la forge et ses projets en y apportant moyens logistiques, humains et financiers.
Le **garant** est un acteur indépendant qui s’assure que les règles de la communauté sont respectées, en l'occurrence ici la DINUM (via sa mission Logiciels libres et communs numériques et son programme Accélérateur d'Initiatives Citoyennes).

## Y a-t-il d'autres forges à l'Éducation nationale ?
Oui. C'est pour cela qu'il est incorrect et source de confusion de qualifier notre forge de « forge de l'Éducation nationale ». C'est pour cela aussi que la notre s'appelle « forge des communs numériques éducatifs", nom un peu long mais qui oriente notre forge vers le pédagogique. 

Il existe en effet, et depuis longtemps, une forge _métiers_ à destination des informaticiens et équipes DSI du ministère et académies. C'est là qu'on y trouve par exemple des applications (pas forcément libres) comme Santorin (pour la correction de copies dématérialisées) ou Afflenet (affectation des élèves en lycée). Ceux qui l'utilisent la nomment souvent "la forge de l'éducation" ou "la forge de Rennes" (car elle est opérée par un pole national qui se trouve à Rennes). Il est bon de l'avoir à l'esprit car certains appellent notre forge "forge de l'Éducation nationale" ignorant l'existence préalable de cette forge et créant sans le vouloir de la confusion. Cette forge est privée, accueillant des projets pas tous libres, projets techniques et non pédagogiques, et elle est non accessible sans identification.

Il y a aussi _la forge mim-libre_ (MIM pour Mutualisation Inter-Ministérielle). Elle est opérée par le Pole de Compétences Logiciels Libres PCLL de Dijon (qui s'occupe aussi de l'administration technique de notre forge). On y trouve des applications (toutes libres pour le coup) comme Eole 3 ou... le portail Apps (pour ceux qui se demandaient où était son code). Elle a vocation à accueillir des applications utiles à tous les ministères (et pas seulement du MEN). A noter que c'est aussi au PCLL qu'on doit l'instance mim-libre de Mastodon qui offre un compte Mastodon aux profs et agents du MEN. Elle est accessible sur https://gitlab.mim-libre.fr/

## La forge plante

Cela peut arriver. La première chose à faire est de voir si c'est général aux services de Apps en vous rendant sur [cette page](https://monitor.eole.education/status/apps-education) de monitoring (lien à conserver dans vos favoris, surtout si vous utilisez d'autres services partagés de Apps). Si tel est le cas alors il n'y a plus qu'à patienter. Sinon vous pouvez signaler le problème sur le [forum Tchap](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr) de la forge.

