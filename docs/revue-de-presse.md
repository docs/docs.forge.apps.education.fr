# Revue de presse

## On parle de LaForgeEdu

- [Que la Forge soit avec vous !](https://drane.ac-normandie.fr/que-la-forge-soit-avec-vous) Hervé Belloc, DRANE Normandie, novembre 2024
- [La Forge des communs numériques éducatifs](https://ressources.dane.ac-versailles.fr/ressource/la-forge-des-communs-numeriques-educatifs) Bar à Ressources, DRANE Versaille, novembre 2024
- [Forge des communs numériques éducatifs](https://pedagogie.ac-clermont.fr/numerique-educatif/2024/11/26/forge-des-communs-numeriques-educatifs/) DRANE Clermont-Ferrand, novembre 2024
- [Découvrir la forge des communs numériques éducatifs](https://www.pedagogie.ac-aix-marseille.fr/jcms/c_11237462/fr/decouvrir-la-forge-des-communs-numeriques-educatifs) DRANE PACA, novembre 2024
- [La forge des communs numériques éducatifs, Co-construire, mutualiser et partager des ressources éducatives libres](https://primabord.eduscol.education.fr/la-forge-des-communs-numeriques-educatifs) Christophe Gilger, Primàbord, octobre 2024
- [Forge des communs numériques](https://dane.ac-reims.fr/index.php/usages-pedagogiques/sinformer/item/455-forge-des-communs-numeriques) Éric Sinet, DRANE site de Reims, octobre 2024
- [La forge des communs numériques éducatifs pour mutualiser nos outils et nos ressources](https://drne.region-academique-bourgogne-franche-comte.fr/la-forge-des-communs-numeriques-educatifs-pour-mutualiser-nos-outils-et-nos-ressources/) Stéphane Fontaine, DRANE Bourgogne-Franche-Comté, octobre 2024
- [La Forge des communs numériques : votre nouvel atelier pour innover en classe](https://dane.site.ac-lille.fr/2024/10/03/la-forge-des-communs-numeriques-votre-nouvel-atelier-pour-innover-en-classe/) Rudy Alba, DRANE Hauts-de-France, octobre 2024
- **[Les pépites de la forge](https://dane.web.ac-grenoble.fr/1d-0/la-forge-quelques-pepites)** Bertrand Chartier et Bruno Morand, DRANE AURA site Grenoble, septembre 2024
- **[La Forge des communs numériques éducatifs : collaborer, mutualiser, innover](https://outilstice.com/2024/06/forge-des-communs-numeriques-educatifs-collaborer-mutualiser/)** Fidel Navamuel (Les Outils Tice), juin 2024
- [La forge des communs numériques éducatifs](http://revue.sesamath.net/spip.php?article1638) Ève Chambon, MathémaTICE, avril 2024

## On cite LaForgeEdu

- [Enseignants référents aux usages du numériques : « Les ERUN bricolent »](https://cafepedagogique.net/2025/01/23/enseignants-referents-aux-usages-du-numeriques-les-erun-bricolent/) Djéhanne Gani, Le Café Pédagogique, janvier 2025
- [Jean-Michel Le Baut : Enfants sauvages du numérique ?](https://cafepedagogique.net/2025/01/06/jean-michel-le-baut-enfants-sauvages-du-numerique/) Claire Berest, Le Café Pédagogique, janvier 2025
- [Un nouveau site Web pour promouvoir les "communs numériques" éducatifs](https://acteurspublics.fr/articles/un-nouveau-site-pour-promouvoir-les-communs-numeriques-educatifs) Xavier Biseul, Acteurs Publics, décembre 2024
- [Lettre ÉduNum SVT n°37](https://eduscol.education.fr/document/61906/download?attachment) Eduscol, Sciences de la vie et de la Terre, novembre 2024
- **[Guide d'accompagnement de la Charte pour l'éducation à la culture et à la citoyenneté numériques](https://eduscol.education.fr/document/61924/download?attachment)** Eduscol, novembre 2024
- [Création, partage, collecte d’activités et générateur d’exercices avec des communs numériques](https://eduscol.education.fr/4146/creation-partage-collecte-d-activites-et-generateur-d-exercices-avec-des-communs-numeriques) Eduscol Mathématiques, novembre 2024
- **[Éducation nationale : les communs numériques, « horizon par défaut »](https://www.zdnet.fr/blogs/education-nationale-les-communs-numeriques-horizon-par-defaut-399778.htm)**, Thierry Noisette, ZDNET France, octobre 2024
- [Guide des Ressources Numériques Éducatives](https://primabord.eduscol.education.fr/guide-des-ressources-numeriques-educatives), Christophe Gilger, Primàbord, octobre 2024
- [Les ressources éducatives libres](https://maison-du-numerique-drane-occitanie.fr/les-ressources-educatives-libres/), Maison du Numérique, octobre 2024
- [vidéo] Audran Le Baron, **[Pourquoi et comment changer nos pratiques pour développer des communs numériques ?](https://tube-numerique-educatif.apps.education.fr/w/bodzGuZRhPecUAg6zHzPrt)** intervention à NEC 2024 à Chambéry ([transcription](https://www.librealire.org/audran-le-baron-pourquoi-et-comment-changer-nos-pratiques-pour-developper-des-communs-numeriques)), septembre 2024
- [« Face à la puissance des Gafam, poursuivons la mise en capacité d’agir du plus grand nombre sur le numérique »](https://www.lemonde.fr/economie/article/2024/09/24/face-a-la-puissance-des-gafam-poursuivons-la-mise-en-capacite-d-agir-du-plus-grand-nombre-sur-le-numerique_6330754_3234.html), Jean Cattan, Le Monde, septembre 2024
- [vidéo] Audran Le Baron, **[discours de cloture](https://podeduc.apps.education.fr/video/42285-journee-du-libre-educatif-a-luniversite-de-creteil/)** de la Journée du Libre Educatif 2024 à Créteil ([transcription](https://www.librealire.org/journee-du-libre-educatif-2024-audran-le-baron)), mars 2024

## On parle des projets de LaForgeEdu

- [La boite à rêves de la ForgeEdu](https://classeadeux.fr/la-boite-a-reves-de-la-forgeedu/) Florent Declas, février 2025
- [Portail primaire de la Forge](https://ressourcesnumeriques91.ac-versailles.fr/spip.php?article197) Aurélie Cuerda, DRANE Versailles, janvier 2025
- [Le portail primaire de la Forge - Des ressources pour l’école primaire](https://primabord.eduscol.education.fr/le-portail-primaire-de-la-forge) Christophe Gilger, Primàbord, novembre 2024
- [QRPrint : un outil open source au service de la pédagogie](https://dane.site.ac-lille.fr/2024/12/01/qrprint-un-outil-open-source-au-service-de-la-pedagogie/) Amélie Silvert, DRANE Lille, décembre 2024
- **[Focus sur les outils numériques à disposition sur le portail primaire La Forge](https://portailecole.scola.ac-paris.fr/informlire-5-decembre/)** Amélie Lemesle, Académie de Paris, décembre 2024
- [CombiCast : un outil simple pour l’enregistrement et le montage audio](https://cafepedagogique.net/2024/11/25/numerique-un-outil-simple-pour-lenregistrement-et-le-montage-audio/) Café Pédagogique, novembre 2024
- [Flouter des visages sur une photo avec Face Privacy](https://drane.ac-normandie.fr/flouter-des-visages-sur-une-photo-avec-face-privacy), Hervé Belloc, DRANE Normandie, novembre 2024
- [Cyber-Enquête](https://scape.enepe.fr/cyber-enquete.html), Mélanie Fenaert, S'CAPE, novembre 2024
- [ViTa : un chatbot avec ou sans IA, créé pour et par les élèves](https://svt.ac-versailles.fr/spip.php?article1355), Mélanie Fenaert, SVT Versailles, novembre 2024
- [Éducajou : des applications libres pour l’école](https://www.libreavous.org/chronique-de-jean-christophe-becquet-sur-educajou-des-applications-libres-pour), émission radio "Libre à vous !", Jean-Christophe Becquet, octobre 2024
- [Un concours pour apprendre le code avec Scratch et Python](https://www.lemondeinformatique.fr/actualites/lire-un-concours-pour-apprendre-le-code-avec-scratch-et-python-94509.html), Véronique Arène, Le Monde Informatique, août 2024
- [podcast] [MADE by ERUN : Arnaud Champollion](https://kiterun.aft-rn.net/made-by-erun-arnaud-champollion/), mai 2024
- [Stéphane Deudon : vers une école du Libre ?](https://cafepedagogique.net/2024/03/27/stephane-deudon-vers-une-ecole-du-libre/), Jean-Michel Le Baut, Café Pédagogique, mars 2024
- [MathALÉA : un must-have pour les profs](https://mathix.org/linux/archives/19193), Julien Durand, Mathix, février 2024
- [Des ressources libres pour la SNT](https://www.class-code.fr/actualites/2024-02-13-des-ressources-libres-pour-la-snt/), Charles Poulmaire, Class Code, février 2024
- [Plongez dans la nuit du code 2024. Une occasion unique de développer les compétences en programmation](https://outilstice.com/2024/01/la-nuit-du-code-2024-developper-competences-en-programmation/), Fidel Navamuel (Les Outils Tice), janvier 2024

## On parle des communs numériques

- **[Soutenir le développement des communs numériques dans l'éducation](https://conf-ng.jres.org/2024/document_revision_1463.html?download)** Thierry Joffredo et Alexis Kauffmann, JRES 2024, décembre 2024
- **[Gazette des communs numériques éducatifs](https://gazette.forge.apps.education.fr/)** Thierry Joffredo et Alexis Kauffmann, n°0 novembre 2024
- [Dossier : Communs numériques pour l'éducation](https://drne.region-academique-bourgogne-franche-comte.fr/dossiers_communs-numeriques/) DRANE Bourgogne-Franche-Comté, novembre 2024
- [Educatech : les communs numériques pour la pérennité des ressources](https://cafepedagogique.net/2024/11/18/educatech-les-communs-numeriques-pour-la-perennite-des-ressources/), Julien Cabioch, Café Pédagogique, novembre 2024
- **[Les communs numériques : un numérique éducatif pour tous et toutes](https://magistere.education.fr/local/magistere_offers/index.php?v=formation#offer=1484)** Parcours Magistère, Réseau Canopé, octobre 2024
- [Développer les communs numériques](https://www.class-code.fr/actualites/2023-05-05-developper-les-communs-numeriques-entretien-avec-alexis-kauffmann-chef-de-projet-a-la-dne/) - Entretien avec Alexis Kauffmann, chef de projet à la DNE, mai 2023, Class Code, mai 2023
- **[La stratégie du numérique pour l'éducation 2023-2027](https://www.education.gouv.fr/strategie-du-numerique-pour-l-education-2023-2027-344263)** - Direction du numérique pour l'éducation, janvier 2023

## On parle des forges

- [État des lieux de la production et de la valorisation des logiciels issus de la recherche publique française](https://www.enseignementsup-recherche.gouv.fr/sites/default/files/2023-12/-tat-des-lieux-de-la-production-et-de-la-valorisation-des-logiciels-issus-de-la-recherche-publique-fran-aise-30402.pdf) Direction Générale de la Recherche et de l’Innovation, ESR, novembre 2023
- **[Forges de l’ESR – Définition, usages, limitations rencontrées et analyse des besoins](https://www.ouvrirlascience.fr/forges-de-lesr-definition-usages-limitations-rencontrees-et-analyse-des-besoins/)** - Daniel Le Berre, Jean-Yves Jeannas, Roberto Di Cosmo, François Pellegrini, Ouvrir la Science, mai 2023
- [vidéo] [L’enjeu des forges souveraines et du logiciel libre](https://www.youtube.com/watch?v=8KQGa6U0HAo), par François Élie ([transcription](https://www.librealire.org/l-enjeu-des-forges-souveraines-et-du-logiciel-libre-par-francois-elie)), 2021
