# Améliorons ensemble les ressources de la forge !

!!! warning "Attention"

    Ce document sera régulièrement mise à jour en fonction de l'expérience et expertise acquises par le groupe.

Bienvenue sur cette **page d'information** des « dindons de la forge », **groupe de test des ressources de la Forge des communs numériques éducatifs**.

Pour rappel la **[forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/)** rassemble et fédère les enseignants, communautés d'enseignants et leurs partenaires qui créent et partagent des logiciels et ressources éducatives libres. On y compte [près de 3000 membres et 2000 projets](https://docs.forge.apps.education.fr/console/) dont 50 sont (fort bien) présentés [sur le site de la DRANE Grenoble](https://dane.web.ac-grenoble.fr/1d-0/la-forge-quelques-pepites).

## En quelques mots

:dart: **L'objectif** de ce groupe est de tester des projets de la forge afin de rendre service aux auteurs en participant à leur amélioration. L'originalité est de **tester ensemble le même logiciel au même moment** (sur une période de 15 jours). Sachant que, lorsque l'actualité l'exige, il sera proposé au groupe de tester également des **nouveautés**.

👫👬👭 Cela permet aux participants de **découvrir** des ressources ainsi que le fonctionnement d'une forge et les avantages du libre, dont la coopération entre utilisateurs et contributeurs. Cela **renforce la communauté de la forge** et témoigne que « l'union fait la forge » n'est pas qu'un slogan ;)

## S'inscrire et participer

💬 **Intéressé•e ?** Alors venez **nous rejoindre** sur le forum Tchap dédié **[TEST LaForgeEdu](https://tchap.gouv.fr/#/room/!FMbqDmwePWphQDroBV:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr)** où l'on échange et coordonne notre action.

🙏 Grand **merci** pour votre éventuelle participation !

!!! failure ""

    ✍️ **[S'INSCRIRE AU GROUPE EN REJOIGNANT LE FORUM TEST DE LA FORGE](https://tchap.gouv.fr/#/room/!FMbqDmwePWphQDroBV:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr)**

## La ressource du moment à tester

La ressource actuellement en test est **Markpage**.

- **[Tester le logiciel](https://lirecouleur.forge.apps.education.fr/weblc6/)**
- [Lire les tickets](https://forge.apps.education.fr/lirecouleur/weblc6/-/issues)
- [Ouvrir un nouveau ticket](https://forge.apps.education.fr/lirecouleur/weblc6/-/issues)

![](https://forge.apps.education.fr/alexis.kauffmann/perso/-/raw/main/lirecouleur-test3.png)



## Planning

Voici la **liste chronologique des logiciels** à tester.

| Ressource                                                                                                                                                 |           Période            |       Auteur(s)        |                                         Descriptif et périmètre                                          |            *Stats* |
| --------------------------------------------------------------------------------------------------------------------------------------------------------- |:----------------------------:|:----------------------:|:--------------------------------------------------------------------------------------------------------:| ------------------:|
| 1 **[Cahiers Numériques](https://www.cahiernum.net/)** ([tickets](https://forge.apps.education.fr/cahiers-numeriques/www-cahiernum-net/-/issues))           | du 14 octobre au 27 octobre  |  Laurent Abbal (AEFE)  |           Côte à côte, un document de travail et un environnement de création (collège, lycée)           | *37 tickets créés* |
| 2 **[Markpage](https://markpage.forge.apps.education.fr/)** ([tickets](https://forge.apps.education.fr/markpage/markpage.forge.apps.education.fr/-/issues)) | du 28 octobre au 8 novembre | Cédric Eyssette (Lyon) | Créer un minisite web ou une application pour smartphone à partir d'un fichier Markdown (collège, lycée) |  *18 tickets créés*                  |
| 🔥¥🔥 3 **[LireCouleur](https://lirecouleur.forge.apps.education.fr/weblc6/)** ([tickets](https://forge.apps.education.fr/lirecouleur/weblc6/-/issues))           | du 9 au 15 novembre |  Marie-Pierre Brungard (Nancy-Metz)  |           Outils destinés à aider les lecteurs débutants à décoder les mots en utilisant les principes de la lecture en couleur (école)           |  |
| 4 **[Manuels libres](https://lesmanuelslibres.region-academique-idf.fr/)** ([tickets](https://forge.apps.education.fr/drane-ile-de-france/les-manuels-libres/accueil-catalogue-manuels-libres/-/issues))           | du 16 au 22 novembre |  Région académique d'Île-de-France (DANE Versaille)  |           16 manuels sous licence libre conçus et partagés par la région académique d'Île-de-France (lycée)           |  |
| 5 **[Seyes](https://educajou.forge.apps.education.fr/seyes/)** ([tickets](https://forge.apps.education.fr/educajou/seyes/-/issues)) | du 23 au 29 novembre | Arnaud-Champollion (Aix-Marseille) | Affiche une page quadrillée de type cahier d'écolier et permet d'y ajouter du texte en police cursive (école) |                    |
| 6 **[ePoc](https://epoc.inria.fr/)** ([tickets](https://forge.apps.education.fr/epoc/epoc-website/-/issues)) | du 30 novembre au 6 décembre | Learning Lab (Inria) | Conception de micro-formations ou contenus pédagogiques ciblés sur smartphone (collège, lycée) |
| 7 **[SACoche](https://sacoche.sesamath.net/)** ([tickets](https://forge.apps.education.fr/sesamath/sacoche/-/issues)) | du 7 au 13 décembre | Sésamath | Application de suivi d'acquisition de compétences (école, collège, lycée) |

Ne pas hésiter à suggérer les prochains projets de la liste dans [notre forum](https://tchap.gouv.fr/#/room/!BXZZsyWklktciNEDbM:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.intradef.tchap.gouv.fr).

## Les nouveautés à tester

Parallélement à la ressource de la quinzaine, le groupe est également sollicité pour tester ponctuellement des nouveautés au moment de leur sortie. 

* **[QRPrint](https://qrprint.forge.apps.education.fr/app/)** ([tickets](https://forge.apps.education.fr/qrprint/app/-/issues)), générateur de QR code avec impression pour un usage facilitée avec les élèves, *par Ulrich Picaud (Grenoble), octobre 2024*
* **[Combicast](https://combicast.forge.apps.education.fr/combicast/)** ([tickets](https://forge.apps.education.fr/combicast/combicast/-/issues)), enregistrer et combiner à la volée plusieurs sons, *par Jérôme Chretinat (Amiens), octobre 2024*

## Bienveillance

L'écrasante majorité des projets de LaForgeEdu provient des enseignants et des communautés enseignantes qui créent et partagent librement (et gratuitement) leurs ressources par dessus leur service de prof.

Il y a un ton, un style, une *délicatesse* à adopter afin notamment que les suggestions d'amélioration ne soient pas prises par les auteurs comme des critiques (*« T'es bien gentil coco avec ta liste au Père Noel, mais je n'ai pas que ça à faire »*).* *« C'est nul, il n'y pas telle fonctionnalité ! »* sera perçu différemment de *« Pour une prochaine version, je verrais bien l'ajout de cette fonctionnalité, si c'était possible, car... »*.

Cela va sans dire, mais c'est mieux en le disant : soyons bienveillants et respectueux dans nos interactions.

## Comment teste-t-on une ressource ?

!!! info

    Cette partie est un premier jet, susceptible de forte évolution.

Chacun est évidemment libre de tester et rendre compte comme il/elle le souhaite. Voici cependant **quelques élements à prendre éventuellement en considération**. Cela s'affinera et s'enrichira au fur et à mesure des avancées du groupe. Peut-être finirons-nous par proposer une sorte de grille commune si on le juge pertinent ?

* **Identification et Cible**
    * Compréhension immédiate de l'objectif et du public cible
    * Pages essentielles : à propos, mentions légales, licence, et contribution (avec lien vers l'espace des tickets du projet dans la forge)
* **Fonctionnalité et Utilisabilité**
    * Efficacité : Le logiciel fait-il ce qu'il promet ?
    * Parcours utilisateur : UX et interface bien conçus (notamment haut et bas de page), fluidité de navigation
* **Compatibilité Multi-Plateforme et Accessibilité**
    * Tests d'affichage : mobile, Windows, Mac, Linux, navigateurs divers
    * Accessibilité : prise en compte des bonnes pratiques (ex. description des images, styles, mode sombre, etc.)
* **Conformité**
    * Conformité au RGPD : préciser pas de données personnelles récoltées/stockées
    * Respect des licences : conformité des ressources incluses
* **Interaction et Support**
    * Facilité à contacter les auteurs
    * Ouverture à la participation et aux contributions de la communauté
    * Documentation, tutoriel, guidages (ex. capsules vidéos)
* **Approche didactique**
    * Pertinence pédagogique
    * Valeur ajoutée du numérique
    * Exemples d'activités et d'usages en classe
* **État du Dépôt du Projet dans la forge (niveau *avancé*)**
    * Vérification des éléments suivants : README, LICENSE (fondamental, avec licence libre pour du code et Creative Commons pour du contenu), CONTRIBUTING (comment contribuer), nom & descriptif, logo, [topics](https://forge.apps.education.fr/explore/projects/topics) (pour faciliter la recherche)
    * Bonnes pratiques de programmation
    * Code bien documenté
    * Performances techniques

## L'Appel à Communs

La DNE est en train de mettre en place un dispositif inédit et innovant de soutien aux communs numériques en général et aux projets de la forge en particulier (en partant du constat que les appels à projets du passé étaient peut adaptés à leurs spécificités).

Lancement prévu : début 2025.

Il comportera notamment un volet financement par prestation externe. L'idée est donc d'en faire bénéficier les projets, notamment ceux qui voudraient développer une fonctionnalité précise (ou améliorer la documentation, l'UX, l'accessibilité, les visuels…) mais qui manquent de temps ou de compétences pour ce faire.

En conséquence, ne pas s'interdire de voir grand dans les suggestions d'amélioration possible envoyées aux auteurs. Quitte à conclure votre message par un *« J'ai conscience que c'est sûrement compliqué à implémenter mais peut-être que dans le cadre de l'appel à communs… »*. 

## Rédiger un ticket

<iframe title="Créer un ticket sur La Forge des Communs Éducatifs - Vidéo Tuto" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/058b9ab7-ab65-4d25-b24f-6a34feda013d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

**La création d'un message sur l'espace ticket du dépôt du projet est le moyen privilégié pour faire des retours aux auteurs des projets** (on dit « ouvrir un ticket », *open an issue* en anglais). Car cela leur permet en un seul endroit d'avoir une vue d'ensemble sur les tâches à évaluer et (éventuellement) accomplir.

Si vous n'en avez jamais ouvert, nous vous conseillons fortement de **voir la vidéo ci-dessus** (2 min, Cyril Iaconelli, CC BY-SA, [source Tube](https://tube-sciences-technologies.apps.education.fr/w/1FHx5ntDrd9mwo5k6dAB2F)). N'hésitez pas également à demander de l'aide [sur le forum Tchap](https://tchap.gouv.fr/#/room/!FMbqDmwePWphQDroBV:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr), certains membres du groupe y étant déjà bien habitués.

*Ci-dessus un exemple d'échange par ticket et son effet sur le logiciel. [Il a été suggéré](https://forge.apps.education.fr/faceprivacy/app/-/issues/1) à l'auteur de [Face Privacy](https://faceprivacy.forge.apps.education.fr/app/), utilitaire permettant de flouter automatiquement les visages d'une image, d'ajouter également le masquage par emoji, ce qu'il a jugé pertinent de faire et a fait dans la foulée en fermant [le ticket](https://forge.apps.education.fr/faceprivacy/app/-/issues/1) (*cose an issue*).*

![](https://nuage03.apps.education.fr/index.php/apps/files_sharing/publicpreview/mYtWbPE44DDYsZW?file=/&fileId=254060016&x=1920&y=1080&a=true&etag=6708dec1c7778)

**Les espaces de tickets ne sont pas des forums de discussion**. On recherche avant tout l'**efficacité**. Un ticket ouvert signifie une tâche éventuelle à accomplir (un bug, une amélioration…).

!!! info

    Important : **si vous avez 3 points différents à retourner aux auteurs, alors créez 3 tickets différents**

Avant de rédiger un ticket, **la première chose à faire est de parcourir l'espace tickets pour voir si d'autres n'ont pas déjà ouvert un ticket similaire** (et si oui, alors pas la peine de créer votre ticket, il vaut mieux participer au ticket déjà créé si vous pensez pouvoir y ajouter quelque chose). Vous pouvez aussi « liker » les tickets des autres pour leur donner plus d'importance.

Il y a principalement deux types de tickets : les **rapports de bug** (*bug report*) et les **demandes d'amélioration** (*feature request*).

Dans tous les cas, soyez clair, précis et concis dans le **choix du titre** de votre ticket (ça aide à s'y retrouver parmi 100 tickets ouverts). La **description** de votre ticket doit être la plus **détaillée** possible (sans se perdre en circonvolutions).

S'il s'agit de rapporter un bug, le décrire précisément (comportement attendu _vs_ comportement observé) ainsi que les actions à réaliser, dans l'ordre, pour le reproduire, en donnant des éléments de contexte (« J'ai testé sur Windows X avec Firefox Y »). Ne pas hésitez à y joindre des documents s'il le faut tels que des **copies d'écran** ou vidéos. Si vous avez une piste de résolution du bug, si vous vous proposez vous-même pour développer la fonctionnalité manquante, il convient évidemment de le signaler (et merci).

Les auteurs du projet parcoureront alors votre ticket et choisiront d'y répondre (ou pas), de le laisser ouvert (ou de le fermer), d'y adjoindre des labels, de l'assigner à un membre de leur groupe, etc.

**Ne pas le prendre mal** si les auteurs prennent du temps avant de répondre à votre ticket, voire carrément n'y répondent pas. Ne pas le prendre mal également si les auteurs ferment votre ticket (jugeant par exemple la demande irréaliste). Leur temps est compté et ils recevront _a priori_ plusieurs tickets en des temps rapprochés. La relation est asymétrique ici : nous sommes bienveillants mais on ne leur demande pas de répondre nécessairement.

Si une personne répond à un ticket que vous avez ouvert, vous en serez **notifié par mail** (paramétrage par défaut de la forge que l'on peut changer, mais c'est sympa d'être tout de suite averti qu'on a lu et répondu à votre ticket)

!!! success

    Ceci étant dit vous pouvez exceptionnellement faire une entorse à la règle et ouvrir un ticket pour remercier les auteurs. Le ticket sera vite fermé mais cela sera sans nul doute apprécié ;)

## L'exemple exemplaire MathALÉA <span>(de Coopmaths)</span>

L'association de profs de maths [Coopmaths](https://coopmaths.fr/www/), développant MathALÉA, est peut-être l'exemple le plus abouti de communauté de la forge (ils sont une vingtaine à travailler directement dedans)
* [MathALÉA](https://coopmaths.fr/alea/) soigne l'UX de son application, avec notamment un pied de page simple et clair (contacts, réseaux sociaux, licence)
* Il existe une page [À propos](https://coopmaths.fr/www/about/) donnant information sur l'équipe et le projet
* Il existe une page [Comment contribuer](https://coopmaths.fr/www/contribuer/) (avec lien direct pour ouvrir un ticket sur la forge)
* Il existe une page [Partenariats](https://coopmaths.fr/www/partenariats/) (mentionnant entre autres LaForgeEdu)
* Il existe une page [Mentions légales](https://coopmaths.fr/www/mentions_legales/) fournissant les nombreuses informations nécessaires, avec notamment toutes les briques open sources utilisées
* Dans le [dépôt MathALÉA](https://forge.apps.education.fr/coopmaths/mathalea) de la forge, le fichier README et le fichier LICENSE sont bien renseignés et [l'usage des tickets](https://forge.apps.education.fr/coopmaths/mathalea/-/issues) est systématisé. Il en ont déjà créé 1521 à ce jour !

![](https://nuage03.apps.education.fr/index.php/apps/files_sharing/publicpreview/a8jTDAfgJAo7TtL?file=/&fileId=253903033&x=1920&y=1080&a=true&etag=67082cdfd7678)

*Copie d'écran de l'espace des tickets de MathALÉA.*

## FAQ

#### Teste-t-on uniquement des logiciels libres ?

Non. Il y a aussi des REL (Ressources éducatives libres) dans la forge : site de contenus pédagogiques [persos](https://eyssette.forge.apps.education.fr/2024/philo-g/) ou [collectifs](https://codex.forge.apps.education.fr/), [jeux sérieux](https://cybersecurite.forge.apps.education.fr/cyber-enquete/), [livres en ligne](https://ada-lelivre.fr/), [manuels](https://lesmanuelslibres.region-academique-idf.fr/), [cahiers de vacances](https://coopmaths.fr/www/cahiers-de-vacances/)… Les logiciels seront plus nombreux mais nous les testerons également.

#### Puis-je tester une autre ressource que celle de la quizaine ?

Bien évidemment ! C'est même l'objectif ultime du groupe : que les utilisateurs d'une ressource de la forge rencontrent les contributeurs en intergissant avec eux via les tickets, et ce à tout moment et pour toutes les ressources. Que cela devienne une sorte de réflexe et une bonne pratique généralisée. De plus, il sera régulièrement et ponctuellement proposé au groupe de tester des nouveautés dès l'annonce de leur sortie, et ce simultanément et parallélement à la ressource de la quinzaine.

#### Je ne suis pas disponible sur la période de test du logiciel.

Aucun problème, vous testerez le suivant si vous en avez le temps. Nous avons pleinement conscience que vous faites cela bénévolement (et nous vous réitérons nos remerciements). Le groupe est _a priori_ suffisamment nombreux pour qu'il y ait de l'activité à chaque fois meme si tout le monde ne participe pas.

#### L'application ne m'intéresse pas. Je ne vois pas comment l'utiliser. C'est pour une autre discipline, je teste quand même ?

Comme dit plus haut, il y a des caractéristiques communes à toute ressource que l'on peut tester et évaluer, indépendamment du champ précis de la ressource. Mais sentez-vous à chaque fois totalement libre de participer ou pas. Ce n'est pas éliminatoire de disparaître ponctuellement.

!!! failure ""

    ✍️ **[S'INSCRIRE AU GROUPE EN REJOIGNANT LE FORUM TEST DE LA FORGE](https://tchap.gouv.fr/#/room/!FMbqDmwePWphQDroBV:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr)**

![](https://nuage03.apps.education.fr/index.php/apps/files_sharing/publicpreview/EgkXNCi6JFaz6SQ?file=/&fileId=253952191&x=1920&y=1080&a=true&etag=6708605b2e662)

*Photos du Forgeathon 2024 qui s'est déroulé le week-end du 5/6 octobre 2024 à Paris. De gauche à droite, haut en bas : la communauté Coopmaths, la communauté PrimTux et la communautés de la forge (formation et gouvernance).*
