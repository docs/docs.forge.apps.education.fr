

# Forgeathon 2024

## Que la forge soit avec nous !

 Le premier Forgeathon organisé par l’[AEIF](https://aeif.fr), s’est tenu les 5 et 6 octobre 2024 dans les locaux de l’[école d’ingénieurs EPITA](https://www.epita.fr/). Plus de 50 participants se sont réunis, et leurs immense énergie, créativité et implication ont contribué à faire de cet événement un véritable succès.

## Déroulé de l’événement

Samedi

- 12h00: Accueil dans une ambiance conviviale autour d'un déjeuner.
- 13h30 : Mot de bienvenue du président de l'AEIF Charles Poulmaire. Présentation de projets par les porteurs ([Codex](https://codex.forge.apps.education.fr), [CoopMath](https://coopmaths.fr), [PrimTux](https://primtux.fr/), Programme de Formation Forge avec [Inria](https://inria.fr), [GTnum Forges](https://lium.univ-lemans.fr/gtnum-forges/), Appel à Communs). Mot de fin à Alexis Kauffmann.

![](forgeathon_2.jpg)

- 13h30 forgeathon (avec 5 groupes : AEIF, Coopmaths, PrimTux, Forge Gouvernance, Forge Formation)

- 18h00 : Regroupement pour partage et bilan de la journée. 

Remarque : CoopMaths était déjà là dès 9h.

Dimanche

- 8h30 : Accueil dans une ambiance conviviale autour d'un petit-déjeuner.

- 9h30 : suite du forgeathon

- 12h : Regroupement pour bilan et perspectives, retours pour améliorer la prochaine édition

- 12h10 Clap de fin dans une ambiance conviviale autour d'un déjeuner.

## Faire communauté

Nous avons ainsi pu poser les premières pierres d’un projet ambitieux, celui de la Forge des Communs Numériques. Ce n’est qu’un début, mais ensemble, nous avons démontré qu’en mettant nos idées et nos compétences en commun, nous pouvons accomplir de grandes choses.

Merci à chacun d'entre vous pour vos contributions, vos échanges, et votre enthousiasme. C’est en construisant collectivement que nous pourrons faire grandir cette communauté et répondre aux défis à venir.

Rendez-vous au Forgeathon 2025, pour de futurs projets !






