# Les pépites de la forge


![Logos des « pépites » de la Forge](pepites2025.png)

Chaque année, le [comité de suivi](/apropos) de la forge des communs numériques éducatifs y sélectionne quelques ressources à mettre en avant et à suivre tout particulièrement.

Parmi les critères de sélection : usages, pédagogie, périmètre, cible, UX, inclusion, communauté, maturité, équilibre et diversité entre les projets.

## Logiciels choisis :

### En 2024

- **[MathALEA](https://coopmaths.fr/alea/)** ([dépôt](https://forge.apps.education.fr/coopmaths/mathalea))
- **[PrimTux](https://primtux.fr/)** ([dépôt](https://forge.apps.education.fr/primtux))

### En 2025

- **[ChatMD](https://eyssette.forge.apps.education.fr/chatMD)** ([dépôt](https://forge.apps.education.fr/eyssette/chatMD)) 
- **[CodEx](https://codex.forge.apps.education.fr)** ([dépôt](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr))
- **[e-comBox](https://www.reseaucerta.org/pgi/e-combox)** ([dépôt](https://forge.apps.education.fr/e-combox))
- **[Educajou](https://educajou.forge.apps.education.fr/)** ([dépôt](https://forge.apps.education.fr/educajou))
- **[La Nuit du Code](https://www.nuitducode.net)** ([dépôt](https://forge.apps.education.fr/nuit-du-code))
- **[LireCouleur](https://lirecouleur.forge.apps.education.fr/)** ([dépôt](https://forge.apps.education.fr/lirecouleur))

Les personnes ou assocations qui les portent sont priorisés dans notre communication, nos invitations et nos dispositifs de soutien et d'accompagnement aux projets de la forge.
