# Faire communauté

Nous œuvrons à favoriser la création et l'animation de communautés autour des projets de la forge par des actions invitant à communiquer, participer et se rencontrer.

## Les forums Tchap pour communiquer

La canal principal d'informations, actualités et échanges (non techniques) de la communauté est notre salon Tchap dédié **[La ForgeEdu](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr)**. C'est aussi l'endroit privilégié pour accueillir les nouveaux venus et celles et ceux qui souhaitent en savoir plus sur le projet. Vous êtes membre de la forge ? Alors nous vous invitons à vous inscrire et participer à ce forum.

-> **[S'inscrire à notre forum Tchap LaForgeEdu](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr)**

Outre ce forum général, nous proposons également :

- un forum d'entraide technique, expertise et développement des projets de la forge : [DEV LaForgeEdu](https://tchap.gouv.fr/#/room/!BXZZsyWklktciNEDbM:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.intradef.tchap.gouv.fr) ;
- un forum spécifiquement dédié aux expérimentation en IA documentées sur le forge : [IA LaForgeEdu](https://tchap.gouv.fr/#/room/!IpZpqVTFcNrhUNUAJc:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.dinum.tchap.gouv.fr&via=agent.culture.tchap.gouv.fr) ;
- un forum dédié à l'école primaire : [Primaire LaForgeEdu](https://tchap.gouv.fr/#/room/!qRbqnWiCvKZLVprqvZ:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.interieur.tchap.gouv.fr).



On notera également que de nombreux projets de la forge ont aussi leur forum sur Tchap comme [PrimTux](https://tchap.gouv.fr/#/room/#Primtux32c6nJqjvWo:agent.education.tchap.gouv.fr), [Educajou](https://tchap.gouv.fr/#/room/!KmbWmUmwIBCWXdjLkT:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.agriculture.tchap.gouv.fr&via=agent.finances.tchap.gouv.fr), [ChatMD](https://tchap.gouv.fr/#/room/!BLAbHlkynUkpyIfNvT:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.dev-durable.tchap.gouv.fr), [e-comBox](https://tchap.gouv.fr/#/room/#ecombox:agent.education.tchap.gouv.fr), [ePoc](https://tchap.gouv.fr/#/room/#epoc:agent.education.tchap.gouv.fr) ou encore [MathALÉA](https://tchap.gouv.fr/#/room/#CoopmathsLVGMDyb1BbF:agent.education.tchap.gouv.fr).

## Le groupe de tests pour participer

Nous avons créé sur Tchap [un groupe](https://tchap.gouv.fr/#/room/!FMbqDmwePWphQDroBV:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.dinum.tchap.gouv.fr) dont l'objectif est de tester des projets de la forge afin de rendre service aux auteurs en participant à leurs améliorations. L’originalité est de tester ensemble le même logiciel au même moment (pendant une semaine).

Cela permet aux participants de découvrir des ressources ainsi que le fonctionnement de la forge (et les avantages du libre), dont la coopération entre utilisateurs et contributeurs. Cela renforce l'esprit de communauté et témoigne que « l’union fait la forge » n’est pas qu’un slogan ;)

-> **[Pour plus d'informations et s'inscrire au groupe](https://docs.forge.apps.education.fr/communaute/test/)**

## Les événements pour se rencontrer

La communauté de la forge se retrouve au cours de l'année lors de différents événement en présentiel, à commencer par le **[« Forgeathon »](https://aeif.fr/index.php/2024/11/07/le-forgeathon-2024/)**, week-end parisien de contribution spécialement dédié à la forge et ses projets.

Nous sommes également fortement présent à la [Journée du Libre Éducatif](https://journee-du-libre-educatif.forge.apps.education.fr/) et à d'autres événements comme [Ludovia](), [NEC](https://numerique-en-communs.fr/) ou encore [Educ@tech](https://www.educatech-expo.com/).