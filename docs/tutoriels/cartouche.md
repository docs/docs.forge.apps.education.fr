# Modèles de cartouches "Forge des Communs numériques éducatifs"

Avec lien vers https://forge.apps.education.fr/
Les styles sont dans le code HTML pour une intégration simple.
Le code à copier contient uniquement le cartouche (le fond affiché sert juste à avoir un aperçu en contexte).

- Pour inclure le cartouche dans un paragraphe existant de votre site, copiez seulement les deux lignes du milieu (sans les `<p> </p>`).

- Pour placer le cartouche dans un nouveau paragraphe, copier tout le code.

## Transparent Clair

<div style="background: linear-gradient(to right, 
                  #ff0000, /* Rouge */
                  #ff7f00, /* Orange */
                  #ffff00, /* Jaune */
                  #00ff00, /* Vert */
                  #0000ff, /* Bleu */
                  #4b0082, /* Indigo */
                  #9400d3 /* Violet */
              );; padding: 15px; height: 150px; border: 2px black solid; border-radius: 10px;">
    
<!-- DÉBUT CODE À COPIER POUR AJOUTER LE CARTOUCHE-->    

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 0.5); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
    
<!-- FIN DU À COPIER POUR AJOUTER LE CARTOUCHE-->       
    
</div>

```
<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 0.5); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
```


## Transparent Sombre

<div style="background: linear-gradient(to right, 
                  #ff0000, /* Rouge */
                  #ff7f00, /* Orange */
                  #ffff00, /* Jaune */
                  #00ff00, /* Vert */
                  #0000ff, /* Bleu */
                  #4b0082, /* Indigo */
                  #9400d3 /* Violet */
              );; padding: 15px; height: 150px; border: 2px black solid; border-radius: 10px;">

<!-- DÉBUT CODE À COPIER POUR AJOUTER LE CARTOUCHE-->    

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #ffffff; background-color: rgba(0, 0, 0, 0.5); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>

<!-- FIN DU À COPIER POUR AJOUTER LE CARTOUCHE-->    
    
</div>

```
<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #ffffff; background-color: rgba(0, 0, 0, 0.5); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
```

## Opaque clair

<div style="background-color: black; padding: 15px; height: 150px; border: 2px black solid; border-radius: 10px;">
    
<!-- DÉBUT CODE À COPIER POUR AJOUTER LE CARTOUCHE-->    

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
    
<!-- FIN DU À COPIER POUR AJOUTER LE CARTOUCHE-->       
    
</div>

```
<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
```

## Opaque clair avec bordure

<div style="background-color: white; padding: 15px; height: 150px; border: 2px black solid; border-radius: 10px;">
    
<!-- DÉBUT CODE À COPIER POUR AJOUTER LE CARTOUCHE-->    

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none; border: 1px black solid;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
    
<!-- FIN DU À COPIER POUR AJOUTER LE CARTOUCHE-->       
    
</div>

```
<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #000000; background-color: rgba(255, 255, 255, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none; border: 1px black solid;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
```


## Opaque Sombre

<div style="background-color: white; padding: 15px; height: 150px; border: 2px black solid; border-radius: 10px;">

<!-- DÉBUT CODE À COPIER POUR AJOUTER LE CARTOUCHE-->    

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #ffffff; background-color: rgba(0, 0, 0, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;position:relative;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin: 2px 5px 2px 2px">Forge des communs numériques éducatifs</a>
</p>

<!-- FIN DU À COPIER POUR AJOUTER LE CARTOUCHE-->    
    
</div>

```
<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #ffffff; background-color: rgba(0, 0, 0, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>

```
## Opaque Sombre avec bordure

<div style="background-color: black; padding: 15px; height: 150px; border: 2px black solid; border-radius: 10px;">

<!-- DÉBUT CODE À COPIER POUR AJOUTER LE CARTOUCHE-->    

<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #ffffff; background-color: rgba(0, 0, 0, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none; border: 1px white solid;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle;  margin: 2px 5px 2px 2px">Forge des communs numériques éducatifs</a>
</p>

<!-- FIN DU À COPIER POUR AJOUTER LE CARTOUCHE-->    
    
</div>

```
<p class="zone-liens" style="text-align: left; margin-top: 15px; margin-bottom: 15px;">
    <a id="laforge" title="La forge des communs numériques éducatifs sur Apps.education.fr" target="_blank" href="https://forge.apps.education.fr/" style="display: inline-block; font-size: 16px; font-family: Arial, Helvetica, sans-serif; text-align: left; font-weight: bold; color: #ffffff; background-color: rgba(0, 0, 0, 1); padding-left: 0px; padding-right: 10px; border-radius: 20px / 50%; text-decoration: none; border: 1px white solid;">
    <img src="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/forge.png" style="height: 40px; width: auto; border-radius: 50%; vertical-align: middle; margin-right: 5px;">Forge des communs numériques éducatifs</a>
</p>
```

