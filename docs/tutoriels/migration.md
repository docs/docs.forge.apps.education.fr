---
hide:
  - navigation
  - toc
---

# Migration d'une forge Gitlab vers LaForgÉdu

!!! note "Un peu d'histoire…"

    Initialement hébergée par l'AEIF, la forge a migré en avril 2024 pour rejoindre les services de Apps Éducation et devenir officiellement la forge des communs numériques éducatifs. La forge de l'AEIF a fermé en novembre 2024, mais ce tutoriel s'applique pour la migration de n'importe quelle instance de Gitlab (comme [gitlab.com](https://gitlab.com) ou [Framagit](https://framagit.org)) vers la Forge.

Si vous n'avez que quelques projets, le plus simple est de les exporter un par un.

## Export d'un projet depuis une instance de Gitlab

Commencez par vous rendre sur la page du projet sur votre instance Gitlab.

![Sélectionner les paramètres](migration/paramètres.png)

![Exporter](migration/exporter.png)

![Attendre le message](migration/export_ok.png)

Si nécessaire, rafraichir la page. Vous devez voir apparaître le bouton Télécharger l'export.

![Télécharger l'export](migration/telecharger.png)

## Importation d'un projet sur forge.apps.education.fr

### Création d'un groupe

Cette opération n'est à faire qu'une seule fois.

![Créer un groupe](migration/nouveau_groupe.png)

Vous devriez arriver sur https://forge.apps.education.fr/groups/new#create-group-pane 

Vous pouvez désormais créer votre groupe

![Création du groupe](migration/creation_groupe.png)

À cet étape, il est essentiel que l'url du groupe soit votre identifiant de l'ancienne forge.

Vous pouvez désormais créer un projet :

![Création d'un projet](migration/creation_projet.png)

Lors de cette phase, vous pourrez importer votre projet :

![Import du projet](migration/importer_projet.png)

Alternativement, si vous êtes familier avec SSH et la ligne de commande : `git push --set-upstream git@forge.apps.education.fr:vincent-xavier.jumel/$(git rev-parse --show-toplevel | xargs basename).git $(git rev-parse --abbrev-ref HEAD)`

Sélectionner « Export gitlab » :

![Import Gitlab](migration/selection.png)

Reprendre désormais le nom de votre ancien projet, sélectionner votre fichier d'export et importez votre projet.

![Import final](migration/import_final.png)

Il faut être patient, l'import peut prendre plusieurs minutes.



!!! note "Exécuter le pipeline au moins une fois"

    Une fois le projet importé, il faut aller dans «Compilation>Pipeline» et l’exécuter au moins une fois. Puis dans «Déploiement > Pages».
    Par défaut, on a : 
    
    ![Domaine unique](migration/domaine_unique.png){ width=70% }


    !!! warning "Important : décocher Utiliser un domaine unique"

        Dans Déploiement > Pages

        👉 Il faut décocher "Utiliser un domaine unique" puis enregistrer cette modification. On obtient alors

        ![Domaine](migration/domaine.png){ width=65% }

        On lit alors l'adresse url du site qui lui est attribuée.

!!! warning "Ne pas oublier de modifier les urls"

    Attention, si vous utilisez un générateur de site web comme `mkdocs`, il faut penser à mettre à jour vos liens, en particulier `site_url`
    au début du fichier  `mkdocs.yml`.



## Gérer la redirection 

Pour rediriger automatiquement vos pages, il suffit d'ajouter une ligne comme `<meta http-equiv="refresh" content="0;url=https://vincentxavier.forge.apps.education.fr/playground/">` dans l'entête html de vos pages web.

Pour les utilisateurs de mkdocs, il faut rajouter dans le répertoire `overrides` un fichier `main.html` avec
```jinja2
{% block extrahead %}
  <!-- gestion de la redirection de la page -->
  <meta http-equiv="refresh" content="0;url={{ [config.site_url,page.url] | join('/') | replace('aeif','apps.education') }}">
  <!-- Fin de la redirection -->
{% endblock %}
```

Si le répertoire `overrides` n'est pas défini, il faut le rajouter dans la configuration, avec la clef `custom_dir: overrides` dans la configuration du thème.

Voir [la documentation de Mkdocs-material](https://squidfunk.github.io/mkdocs-material/customization/#overriding-blocks).
