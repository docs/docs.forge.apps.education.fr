# Tutoriels

En attendant de développer, peaufiner, ordonner cette partie, voici quelques tutoriels :

## Utiliser la Forge

* [vidéo] [Bien commencer avec la Forge](https://tube-numerique-educatif.apps.education.fr/w/vAMyPdtMNRPe8c4TuqhX1d){:target="_blank" } (comment créer un groupe ou un projet, ajouter et modifier des éléments, publier un site) par Bertrand Chartier (DANE Grenoble)
* [vidéo] [Comment créer un ticket sur la forge](https://tube-sciences-technologies.apps.education.fr/w/1FHx5ntDrd9mwo5k6dAB2F) permettant aux auteurs d'un projet de bénéficier de vos retours d'usage (rapport de bug, suggestion d'amélioration, etc.), 2 min, par Cyril Iconelli (ac. Rennes)
* [vidéo] [Contribuer à la forge des communs numériques éducatifs avec les tickets](https://tube-numerique-educatif.apps.education.fr/w/5JWEYw4HNgUdZphuaCquvW) permettant de découvrir la forge, s'y connecter et utiliser les tickets pour contribuer, 6 min, par Hervé Allesant (ac. Aix-Marseille)
* [Utiliser la Forge avec VSCodium ou Visual Studio Code](https://forge.apps.education.fr/laurent.abbal/utiliser-la-forge-avec-vscodium-ou-visual-studio-code){:target="_blank" }

## Créer un site web

* [Tutoriels et aides pour l'enseignant qui crée son site avec Python](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/)
* [Tutoriels et aides pour l'enseignant qui crée son site simple](https://docs.forge.apps.education.fr/modeles/tutoriels/tutoriel-site-simple/)
* [Comment créer un site à partir d'un autre ?](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/08_tuto_fork/1_fork_projet/)

## Créer une application

* [vidéo] [Créer une application avec l'IA ](https://tube-numerique-educatif.apps.education.fr/w/9hazJSmadP9RWYpeotxbBv) par Herve Allesant (ac. Aix-Marseille)

## Modèles

Voici quelques modèles pour commencer à utiliser la Forge des Communs Numériques Éducatifs, à *forker* :

<div class="grid cards" markdown>

-   **Diaporama en Markdown**

    ---

    Un modèle pour réaliser des diaporamas en markdown avec de nombreuses fonctionnalités pratiques.

    [:material-web-box: Exemple](https://docs.forge.apps.education.fr/modeles/diaporama-en-markdown/){ .md-button }
    [:material-source-repository: Dépôt](https://forge.apps.education.fr/docs/modeles/diaporama-en-markdown){ .md-button }
    [:material-help-box: Doc](https://forge.apps.education.fr/docs/modeles/diaporama-en-markdown/-/blob/main/README.md){ .md-button }

-   **Une seule page HTML**

    ---

    Un outil qui transforme automatiquement vos fichiers Markdown en pages HTML, utile pour réaliser un tout petit site web très simple d'une seule page web.

    [:material-web-box: Exemple](https://docs.forge.apps.education.fr/modeles/modele-pandoc/){ .md-button }
    [:material-source-repository: Dépôt](https://forge.apps.education.fr/docs/modeles/modele-pandoc){ .md-button }
    [:material-help-box: Doc](https://forge.apps.education.fr/docs/modeles/modele-pandoc/-/blob/main/README.md){ .md-button }

-   **Site web simple**

    ---

    Un site web simple utilisant le moteur [Jekyll](https://jekyllrb.com/).

    [:material-web-box: Exemple](https://pages.gitlab.io/jekyll/){ .md-button }
    [:material-source-repository: Dépôt](https://forge.apps.education.fr/docs/modeles/modele-jekyll){ .md-button }
    [:material-help-box: Doc](https://forge.apps.education.fr/docs/modeles/modele-jekyll/-/blob/main/README.md){ .md-button }

-   **Site web de cours à usage général**

    ---

    Un site web un peu plus complexe.

    [:material-web-box: Exemple](https://docs.forge.apps.education.fr/modeles/site-web-cours-general/){ .md-button }
    [:material-source-repository: Dépôt](https://forge.apps.education.fr/docs/modeles/site-web-cours-general){ .md-button }
    [:material-help-box: Doc](https://docs.forge.apps.education.fr/modeles/tutoriels/tutoriel-site-simple/){ .md-button }

-   **Site web de cours avec exercices Python dans le navigateur**

    ---

    Comme le précédent, mais permet en plus de proposer des exercices en Python qui s'exécutent dans le navigateur.

    [:material-web-box: Exemple](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/){ .md-button }
    [:material-source-repository: Dépôt](https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review){ .md-button }
    [:material-help-box: Doc](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/){ .md-button }

-   **Compilation de documents $\LaTeX$**

    ---

    Un exemple de fichier `.gitlab-ci.yml` pour compiler des fichiers $\LaTeX$ à la volée lors de l'ajout ou de la modification de nouveaux fichiers `.tex`.

    [:material-source-repository: Dépôt](https://forge.apps.education.fr/docs/modeles/latex){ .md-button }

</div>

Et du code à copier-coller :

* [Citer la Forge](cartouche.md)

## Migrer ses projets depuis une autre forge

* [Migrer des projets depuis une instance Gitlab](migration.md)
* [Import et synchro GitHub - GitLab](https://forge.apps.education.fr/laurent.abbal/import-et-synchro-github-gitlab)
