# À propos…

La forge des communs numériques éducatifs est opérée par la [Direction du numérique pour l'éducation](https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983) (DNE), avec le soutien de la [Région académique Auvergne-Rhône-Alpes](https://www.ac-lyon.fr/la-region-academique-auvergne-rhone-alpes-121621) (AURA), en partenariat avec l'[Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/) (AEIF).

Cette organisation tripartite (national, territorial et associatif) assure une gouvernance partagée au projet.

Opérateur de la forge, le comité de suivi (COSUI) est composé des personnes suivantes :

- Laurent Abbal, professeur de NSI/Physique-Chimie et RRUPN au Lycée Français International de Tokyo (AEFE), professeur de Physique à l'université Keiō (Tokyo)
- Franck Bodin, chef de projet communs numériques et mixité à la DNE
- Arnaud Champollion, conseiller pédagogique numérique 1er degré, circonscription de Digne-les-Bains
- Bertrand Chartier, enseignant et chargé de mission à la DRANE Académie de Grenoble
- Mireille Coilhac, professeure de NSI et mathématiques en lycée et BTS dans l'académie de Créteil
- Perrine Douhéret, professeure de SVT et chargée de projet à la DRANE site de Lyon
- Cédric Eyssette, professeur de philosophie et chargé de projet à la DRANE site de Lyon
- Caroline Guedan, cheffe de projet ressources numériques à la DNE
- Matthieu Guérin, chef de projet interopérabilité et services numériques pour l'éducation à la DNE
- Cyril Iaconelli, professeur de mathématiques et RRUPN au collège dans l'académie de Rennes
- Vincent-Xavier Jumel, professeur de mathématiques et informatique dans l'académie de Paris
- Alexis Kauffmann, chef de projet logiciels et ressources éducatives libres à la DNE
- Louis Paternault, professeur de mathématiques
- Charles Poulmaire professeur de mathématiques et informatique, formateur académique en mathématiques et informatique. Interlocuteur académique au numérique (IAN) en NSI de l'académie de Versailles, président de l'AEIF

La forge fait partie des services numériques partagés de [apps.education.fr](https://portail.apps.education.fr/signin). Elle repose sur le logiciel libre [GitLab](https://fr.wikipedia.org/wiki/GitLab) CE et est infogérée par le [PCLL](https://pcll.ac-dijon.fr/) (pôle de compétences Logiciels Libres).

## Objectifs 

Le COSUI de LaForgeEdu a trois principaux objectifs :

1. démocratiser l'outil et monter en compétences numériques
1. faire communauté en incitant les utilisateurs à devenir contributeurs
1. valoriser les projets et accompagner certains d'entre eux à passer à l'échelle


## Historique

- 2024
    - 1.3 mise à jour du site présentant la forge + cartographie des projets (novembre)
    - 1.2 forgeathon (octobre)
    - 1.1 démarrage du plan de formation forge (septembre)
    - **1.0 JDLE 2024, arrivée de la forge dans Apps (mars)**
- 2023
    - 0.3 JDLE 2023, premier atelier dédié à la forge (mars)
    - 0.2 la forge citée comme objectif de la Stratégie du numérique pour l'éducation 2023-2027 (janvier)
- 2022
    - **0.1 création de la forge de l'AEIF chez CleverCloud (mars)**
    - 0.0 L'AEIF exprime le besoin à la DNE de travailler sur une forge (janvier)

## Événements

- [Ça bouge (enfin) à l’Éducation nationale !](https://www.samedis-du-libre.org/Les-conferences) Les samedis du libre, Paris, décembre 2024
- [Des logiciels libres utiles à tous conçus dans la forge des communs numériques de apps.education.fr](https://code.gouv.fr/fr/bluehats/journees-2024/) Open Source Experience, Paris, décembre 2024
- [Mercredis du numérique : construire un commun numérique au service des enseignants 1/2 : Le cas de LaForgeEdu](https://drne.region-academique-bourgogne-franche-comte.fr/mercredis-du-numerique-construire-un-commun-numerique-au-service-des-enseignants-1-2-le-cas-de-laforgeedu/) ([replay vidéo](https://tube-numerique-educatif.apps.education.fr/w/16oegrMig9S6dfe56edw9a)) DRANE Bourgogne-Franche-Comté, novembre 2024
- [Educ@tech Expo](https://www.linkedin.com/posts/direction-du-num%C3%A9rique-pour-l-%C3%A9ducation_dne-ia-magistaeyre-activity-7259923481683795970-fxVB), Paris, novembre 2024

## Contact

- Si vous êtes enseignante ou enseignant, vous pouvez échanger avec nous publiquement dans [le forum Tchap dédié à la forge](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr).
- Si vous êtes membre de la forge, vous pouvez [ouvrir un ticket](https://forge.apps.education.fr/docs/support/-/issues).
- Vous pouvez aussi nous contacter directement à l’adresse suivante : `forge-communs-numeriques@ldif.education.gouv.fr`.

## Licence

Sauf mention contraire, les contenus de ce site web sont publiés sous [licence etalab-2.0](https://github.com/etalab/licence-ouverte/blob/master/LO.md).
